# Consistent Feature Model Evolution Plans

Online Appendix for SPLC'20 submission "*Consistency-Preserving Evolution Planning on Feature Models*" by A. Hoff, M. Nieke, C. Seidl, E. H. Sæther, I. S. Motzfeldt, C. Chang Din, I. Chieh Yu, and I. Schaefer.

**DarwinSPL** Eclipse Plug-Ins: [https://gitlab.com/DarwinSPL/DarwinSPL](https://gitlab.com/DarwinSPL/DarwinSPL)

Link to the Maude implementation of our execution semantics: [https://github.com/idamotz/LTEP-summer-project](https://github.com/idamotz/LTEP-summer-project)

*  Entire list of semantics for feature model deit operations: [edit-operations-semantics.pdf](https://gitlab.com/Adomat/consistent-feature-model-evolution-plans/-/blob/master/edit-operations-semantics.pdf)
*  Overview of how edit operations can cause varios paradoxes: [edit-operations-to-paradoxes.pdf](https://gitlab.com/Adomat/consistent-feature-model-evolution-plans/-/blob/master/edit-operations-to-paradoxes.pdf)
*  Entire syntax of our user-level FMEC language: [fmec-syntax.pdf](https://gitlab.com/Adomat/consistent-feature-model-evolution-plans/-/blob/master/edit-operations-semantics.pdf)
